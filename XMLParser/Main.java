public class Main {
  public static void main(String[] args) {
    new XMLParse();

    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        new GUI();
      }
    });
  }
}
