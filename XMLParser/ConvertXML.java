import java.io.*;

public class ConvertXML {
  public ConvertXML() {
    convertToText();
  }

  private void convertToText() {
    try {
      BufferedReader br = new BufferedReader(new FileReader("contacts.xml"));
      FileWriter fw = new FileWriter("contacts.txt");
      String line = "";

      while((line = br.readLine()) != null) {
        line = line.trim();
        fw.write(line);
      }
      br.close();
      fw.close();

    } catch (IOException io) {
      System.out.println(io.getMessage());
    }
  }
}
