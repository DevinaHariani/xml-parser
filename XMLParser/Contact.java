public class Contact {
  private String name, phone, email, dob = "";

  public Contact() {
  }

  public Contact(String name, String phone, String email, String dob) {
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.dob = dob;
  }

  public String[] getArray() {
    String[] dataArray = { name, phone, email, dob };
    return dataArray;
  }

  public String getName() {
    return name;
  }

  public String getPhoneNumber() {
    return phone;
  }

  public String getEmail() {
    return email;
  }

  public String getDOB() {
    return dob;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPhoneNumber(String phone) {
    this.phone = phone;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setDOB(String dob) {
    this.dob = dob;
  }

  @Override
  public String toString() {
    return "Contact: [" + this.name + " , " + this.phone + " , " + this.email + " , " + this.dob + "] ";
  }
}