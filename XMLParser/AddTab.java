import javax.swing.*;
import java.awt.event.*;

public class AddTab extends JPanel implements ActionListener {
  JTextField name, phone, email, dob;
  JButton submit;
  GUI obj;

  public AddTab(GUI obj) {
    this.obj = obj;

    // setBounds(27, 30, 93, 15);
    add(new JLabel("Enter your name:"));

    // setBounds(157, 30, 100, 15);
    add(name = new JTextField(15));

    // setBounds(27, 61, 93, 51);
    add(new JLabel("Enter your phone number:"));

    // setBounds(158, 61, 100, 15);
    add(phone = new JTextField(15));

    // setBounds(27, 92, 93, 51);
    add(new JLabel("Enter your email:"));

    // setBounds(200, 200, 300, 15);
    add(email = new JTextField(15));

    // setBounds(27, 123, 93, 51);
    add(new JLabel("Enter your dob:"));

    // setBounds(215, 220, 100, 15);
    add(dob = new JTextField(15));

    // setBounds(215, 300, 300, 15);
    add(submit = new JButton("SUBMIT"));
    submit.addActionListener(this);
  }

  public void actionPerformed(ActionEvent ae) {
    ContactsList.addContact(new Contact(name.getText(), phone.getText(), email.getText(), dob.getText()));
    JOptionPane.showMessageDialog(this, "Your contact was successfully added", "SUCCESS!",
        JOptionPane.INFORMATION_MESSAGE);
    obj.updateContactsTab();
    name.setText("");
    phone.setText("");
    email.setText("");
    dob.setText("");
  }
}
