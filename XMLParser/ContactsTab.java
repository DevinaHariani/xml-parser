import javax.swing.*;
import java.awt.BorderLayout;

public class ContactsTab extends JPanel {
  public ContactsTab() {
    setLayout(new BorderLayout());
    String[][] data = ContactsList.get2dArray();
    String[] colHeads = {"Name", "Phone Number", "Email", "DOB"};

    JTable table = new JTable(data, colHeads);
    int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
    int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
    JScrollPane jsp = new JScrollPane(table, v, h);
    add(jsp);
  }
}
