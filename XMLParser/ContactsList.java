import java.util.*;

public class ContactsList {
  private static ArrayList<Contact> contacts = new ArrayList<>();
  Contact c = new Contact();

  public static Contact getContact(int index) {
    return contacts.get(index);
  }

  public static String[][] get2dArray() {
    String[][] twoDimensionalArray = new String[ContactsList.getSize()][];
    for(int i = 0; i < ContactsList.getSize(); i++) {
      twoDimensionalArray[i] = ContactsList.getContact(i).getArray();
    }
    return twoDimensionalArray;
  }

  public static String getName(int index) { return contacts.get(index).getName(); }
  public static String getPhone(int index) { return contacts.get(index).getPhoneNumber(); }
  public static String getEmail(int index) { return contacts.get(index).getEmail(); }
  public static String getDOB(int index) { return contacts.get(index).getDOB(); }

  public static int getSize() {
    return contacts.size();
  }

  
  public static void addContact(Contact c) {
    contacts.add(c);
    new ContactToXML();
    new ContactsTab();
  }

  public static void deleteContact(String name) {
    for (int i = 0; i < contacts.size(); i++) {
      if (contacts.get(i).getName().equals(name)) {
        contacts.remove(i);
      }
    }
    new ContactToXML();
  }

  public static void updateContact(String searchName, String updateName, String updatePhone, String updateEmail, String updateDOB) {
    for (int i = 0; i < contacts.size(); i++) {
      if (contacts.get(i).getName().equals(searchName)) {
        contacts.get(i).setName(updateName);
        contacts.get(i).setPhoneNumber(updatePhone);
        contacts.get(i).setEmail(updateEmail);
        contacts.get(i).setDOB(updateDOB);
      }
    }
    new ContactToXML();
  }

  public static void showContacts() {
    for (int i = 0; i < contacts.size(); i++) {
      System.out.println(contacts.get(i));
    }
  }
}
