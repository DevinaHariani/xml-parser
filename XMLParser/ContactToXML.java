import java.io.*;

public class ContactToXML {
  public ContactToXML() {
    String xml = "<contact-list>";

    for (int i = 0; i < ContactsList.getSize(); i++) {
      Contact c = ContactsList.getContact(i);
      xml += "<contact><name>" + c.getName() + "</name>";
      xml += "<phone>" + c.getPhoneNumber() + "</phone>";
      xml += "<email>" + c.getEmail() + "</email>";
      xml += "<dob>" + c.getDOB() + "</dob></contact>";
    }

    xml += "</contact-list>";

    try {
      PrintWriter pw = new PrintWriter(new FileOutputStream("contacts.xml", false), false);
      pw.println(xml);
      pw.flush();
      pw.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
} 