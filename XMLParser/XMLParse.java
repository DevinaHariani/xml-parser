import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLParse {
  String regex = "(?:<contact>)([\\w\\d\\s<>\\/\\.@-]*?)(?:<\\/contact>)";
  String line = "";

  public XMLParse() {
    new ConvertXML();
    parseXML();
  }

  private void parseXML() {
    try {
      BufferedReader br = new BufferedReader(new FileReader("contacts.txt"));
      line = br.readLine();
      Pattern pattern = Pattern.compile(regex);
      Matcher matcher = pattern.matcher(line);

      while (matcher.find()) {
        String str = matcher.group(1);
        Matcher m = Pattern.compile(
            "(?:<name>)([\\w\\d\\s<>\\/\\.@-]*?)(?:<\\/name>)(?:<phone>)([\\d<>]*?)(?:<\\/phone>)(?:<email>)([\\w\\d<>\\/\\.@-]*?)(?:<\\/email>)(?:<dob>)([\\w\\d<>\\/\\.@-]*?)(?:<\\/dob>)")
            .matcher(str);

        if (m.find()) {
          ContactsList.addContact(new Contact(m.group(1), m.group(2), m.group(3), m.group(4)));
        }
      }
      br.close();
      new File("contacts.txt").delete();
    } catch (IOException io) {
      System.out.println(io.getMessage());
    }
  }
}
